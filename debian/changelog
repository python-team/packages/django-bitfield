django-bitfield (2.2.0-2) unstable; urgency=medium

  * Team upload.
  * Use upstream commit to remove dependency on python3-six

 -- Alexandre Detiste <tchet@debian.org>  Sun, 12 Jan 2025 18:46:08 +0100

django-bitfield (2.2.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 2.2.0
    which fixes assertEquals -> assertEqual
  * switch from nose to pytest (Closes: #1018339)
  * add upstream patch #131 to fix psycopg3 compatibility
  * switch to watch file v4
  * set Rules-Requires-Root: no
  * bump Standards-Version: to 4.6.2
  * use new dh-sequence-python3

 -- Alexandre Detiste <tchet@debian.org>  Sat, 20 Jan 2024 12:10:36 +0100

django-bitfield (1.9.6-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from deprecated 9 to 12.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.

 -- Sandro Tosi <morph@debian.org>  Sat, 11 Jun 2022 22:01:54 -0400

django-bitfield (1.9.6-2) unstable; urgency=medium

  * Team upload.
  * Reupload without binaries to allow migration.

 -- Andrey Rahmatullin <wrar@debian.org>  Wed, 07 Aug 2019 12:42:47 +0500

django-bitfield (1.9.6-1) unstable; urgency=low

  * Team upload.
  * New upstream release.
  * Add debian/gbp.conf.
  * Bump debhelper version to 12.
  * Bump Standards-Version to 4.4.0.
  * Run wrap-and-sort -bast to reduce diff size of future changes.

 -- Michael Fladischer <fladi@debian.org>  Tue, 23 Jul 2019 10:55:58 +0200

django-bitfield (1.9.3-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/watch: Use https protocol
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.

  [ Thomas Goirand ]
  * Team upload.
  * Removed Python 2 support.

 -- Thomas Goirand <zigo@debian.org>  Mon, 22 Jul 2019 03:38:23 +0200

django-bitfield (1.9.3-1) unstable; urgency=low

  * Team upload.
  * New upstream release.
  * Bump Standards-Version to 4.0.0.
  * Reformat packaging files with cme for better readability.

 -- Michael Fladischer <fladi@debian.org>  Tue, 20 Jun 2017 10:43:49 +0200

django-bitfield (1.8.0-1) unstable; urgency=low

  * New upstream release.
  * Add Python3 support through a separate package.
  * Bump Standards-Version to 3.9.6.
  * Bump debhelper compatibility level to 9.
  * Use pybuild to build both Python2 and Python3 packages.
  * Use pypi.debian.net service for uscan.
  * Drop versioned dependency on python-setuptools and python-all.
  * Add dh-python to Build-Depends.
  * Add python-six to Build-Depends.
  * Add Python3 variants of all build dependencies.
  * Add X-Python-Version and X-Python3-Version fields.
  * Add homepage field.
  * Add Vcs-* fields for git repository.
  * Add Upstream-Contact field to d/copyright.
  * Add myself in d/copyright.
  * Disable tests for now as they require a running database server.
  * Reformat d/control and d/copyright with cme.

 -- Michael Fladischer <fladi@debian.org>  Fri, 26 Jun 2015 11:03:04 +0200

django-bitfield (1.6.4-1) unstable; urgency=low

  * Initial release. (closes: #702464)

 -- Luke Faraone <lfaraone@debian.org>  Wed, 06 Mar 2013 16:00:31 -0500
